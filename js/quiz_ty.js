var QuizTY = QuizTY || {};

/**
 * Adding behavior. Behaviors are called everytime a page is refreshed fully or through ahah.
 */
Drupal.behaviors.quizTYBehavior = function(context) {
  QuizTY.activateNavigation();
};

/**
 * Init variables in the QuizTY singleton
 */
QuizTY.initVariables = function() {
  QuizTY.loadNumber = 5; // How many questions we load each time
  QuizTY.processed ='quizTYBehavior-processed'; 
  QuizTY.notProcessed = ':not(.quizTYBehavior-processed)'; 
  QuizTY.lastQuestionLoaded = -1; // We haven't loaded any questions yet.
  QuizTY.numLeftWhenLoading = 1; // How many questions should we have ready when we start loading more?
  QuizTY.isLoading = false;
  QuizTY.waitingForNextQuestion = false;
};

/**
 * Store the questions that should belong to this quiz, and starts the quiz taking process.
 */
QuizTY.setQuestions = function(questionIds, quizNid) {
  QuizTY.initVariables();
  QuizTY.questions = questionIds;
  QuizTY.quizNid = quizNid;
  QuizTY.urlStart = location.protocol + '//' + location.host + Drupal.settings.basePath + "quiz_ty/ajax/";
  QuizTY.currentQuestion = 0;
  Drupal.attachBehaviors();
  QuizTY.parseQuestionData(1);
  QuizTY.lastQuestionLoaded = 0;
  QuizTY.showCurrentQuestion();
};

/**
 * Loads the next questions from the server using ajax
 */
QuizTY.loadNextQuestions = function() {
  var numLoad = Math.min(QuizTY.loadNumber, (QuizTY.questions.length - QuizTY.lastQuestionLoaded - 1));
  var questionsToLoad = new Array();
  for (var i = QuizTY.lastQuestionLoaded + 1; i < QuizTY.lastQuestionLoaded + numLoad + 1; i++) {
    questionsToLoad.push(QuizTY.questions[i]);
  }
  QuizTY.loadQuestions(questionsToLoad);
};

/**
 * Show the next question.
 * 
 * If the next question hasn't been loaded this function will show the preloader
 */
QuizTY.nextQuestion = function(event) {
  if (QuizTY.lastQuestionLoaded > QuizTY.currentQuestion) {
    QuizTY.currentQuestion++;
    QuizTY.showCurrentQuestion();
  }
  else {
    QuizTY.waitForNextQuestion();
  }
};

/**
 * Show the previous question
 */
QuizTY.prevQuestion = function(event) {
  QuizTY.currentQuestion--;
  QuizTY.showCurrentQuestion();
};

/**
 * Mark all questions.
 * 
 * Also shows the result report
 */
QuizTY.markQuestions = function(event) {
  QuizTY.totalScore = 0;
  QuizTY.totalMax = 0;
  for (var i in QuizTY.questions) {
    var question = QuizTY.questions[i];
    question.o.mark(question);
    QuizTY.totalScore += question['score'];
    QuizTY.totalMax += question['relative_max_score'];
  }
  var toReturn = QuizTY.theme_report();
  $('#quiz_progress').hide();
  $('#question-section').after(toReturn).hide();
};

/**
 * Activates the navigation buttons
 */
QuizTY.activateNavigation = function() {
  $('#next-question' + QuizTY.notProcessed).click(QuizTY.nextQuestion).addClass(QuizTY.processed);
  $('#previous-question' + QuizTY.notProcessed).click(QuizTY.prevQuestion).addClass(QuizTY.processed);
  $('#mark-questions' + QuizTY.notProcessed).click(QuizTY.markQuestions).addClass(QuizTY.processed);
};

/**
 * Shows the preloader and registers that we are waiting for the next question.
 */
QuizTY.waitForNextQuestion = function() {
  $('#loading-questions').show();
  QuizTY.waitingForNextQuestion = true;
  QuizTY.deactivateNavigation();
};

/**
 * One or more questions have been loaded and parsed
 */
QuizTY.newQuestionsReady = function() {
  $('#loading-questions').hide();
  if (QuizTY.waitingForNextQuestion) {
    QuizTY.activateNavigation();
    QuizTY.nextQuestion();
  }
  QuizTY.waitingForNextQuestion = false;
};

/**
 * Deactivate navigation buttons
 */
QuizTY.deactivateNavigation = function() {
  $('.navigation').removeClass(QuizTY.processed).unbind('click');
};

/**
 * Load questions using ajax
 * 
 * @param questions
 *  array of question ids for the questions we want to load
 */
QuizTY.loadQuestions = function(questions) {
  if (QuizTY.isLoading) { 
    return;
  }
  var toSend = '';
  for(var i in questions) {
    toSend += '&' + i + '[nid]=' + questions[i]['nid'];
    toSend += '&' + i + '[vid]=' + questions[i]['vid'];
  }
  var url = QuizTY.urlStart + QuizTY.quizNid + '/load_questions';
  $.post(url, toSend, QuizTY.recieveQuestions);
  QuizTY.isLoading = true;
};

/**
 * Callback function used when one or more questions have been loaded.
 * 
 * @param data
 *  Data sent from the server.
 */
QuizTY.recieveQuestions = function(data) {
  QuizTY.isLoading = false;
  var json = Drupal.parseJson(data);
  var selector = '#quiz-question-answering-form-' + QuizTY.questions[QuizTY.currentQuestion]['nid'];
  $(selector).after(json.data);
  QuizTY.parseQuestionData(json.num_loaded);
  QuizTY.lastQuestionLoaded += json.num_loaded;
  QuizTY.showCurrentQuestion();
  Drupal.attachBehaviors();
  QuizTY.newQuestionsReady();
};

/**
 * Shows the current question and hides all the others
 * 
 * Also handles some other logic
 */
QuizTY.showCurrentQuestion = function() {
  // Start loading more questions if needed
  var allLoaded = QuizTY.lastQuestionLoaded >= QuizTY.questions.length - 1;
  var notEnoughLeft = QuizTY.currentQuestion >= QuizTY.lastQuestionLoaded - QuizTY.numLeftWhenLoading; 
  if (!allLoaded && notEnoughLeft) {
    QuizTY.loadNextQuestions();
  }
  
  // Hide all the other questions
  var selector = '#quiz-question-answering-form-' + QuizTY.questions[QuizTY.currentQuestion]['nid'];
  $('.answering-form:not(' + selector + ')').hide();
  
  // Show the current question
  $(selector).show();
  
  // Decide what buttons to hide and what buttons to show
  if (QuizTY.currentQuestion > QuizTY.questions.length - 2) {
    $('#next-question').hide();
    $('#mark-questions').show();
  }
  else {
    $('#next-question').show();
    $('#mark-questions').hide();
  }
  if (QuizTY.currentQuestion > 0) {
    $('#previous-question').show();
    $('#previous-question-inactive').hide();
  }
  else {
    $('#previous-question').hide();
    $('#previous-question-inactive').show();
  }
  $('#quiz-question-number').html((QuizTY.currentQuestion + 1));
};

/**
 * Parse the question data into the questions array
 * 
 * @param numLoad
 *  The number of questions to load data for
 */
QuizTY.parseQuestionData = function(numLoad) {
  for (var i = QuizTY.lastQuestionLoaded + 1; i <= QuizTY.lastQuestionLoaded + numLoad; i++) {
    var question = QuizTY.questions[i];
    $.extend(question, Drupal.parseJson($('#edit-ty-data-' + question['nid']).val()));
    
    // Store a reference to the questions type-specific singleton
    question.o = QuizTY.objectForType(question['type']);
    
    // parse type specific data
    question.o.parseQuestionData(question);
    
    // Store the question html in the questio object
    var questionSelector = '#quiz-question-answering-form-' + question['nid'];
    question['question'] = '<div class="quiz-question-body">' + $(questionSelector + ' .quiz-question-body').html() + '</div>';
    
    // Parse the relative max_score
    question['relative_max_score'] = parseInt(question['relative_max_score']);
    
    // find the score weight
    if (question['max_score'] == 0) {
      question['score_weight'] = 0;
    }
    else {
      question['score_weight'] = question['relative_max_score'] / question['max_score'];
    }
  }
};

/**
 * Return the correct singleton for a question type
 * 
 * @return
 *  reference to a singleton
 */
QuizTY.objectForType = function(type) {
  var objects = {
    multichoice: Multichoice
  };
  return objects[type];
};

var Multichoice = Multichoice || {};

/**
 * Parse multichoice specific data
 */
Multichoice.parseQuestionData = function(question) {
  // We need to parse a lot of data from string to int
  var integers = ['max_score', 'choice_multi', 'choice_random', 'choice_boolean'];
  for (var i in integers) {
    question[integers[i]] = parseInt(question[integers[i]]);
  }
  integers = ['score_if_chosen', 'score_if_not_chosen'];
  for (i in question['alternatives']) {
    for (var j in integers) {
      question['alternatives'][i][integers[j]] = parseInt(question['alternatives'][i][integers[j]]);
    }
  }
};

/**
 * Generates a result report for a question
 *
 * The results are stored in the question object
 *
 * @param question
 *  A question object holding all relevant data for the question
 */
Multichoice.mark = function(question) {
  var alternatives = question['alternatives'];
  
  // Find the alternative with the highest score
  if (question['choice_multi'] == 0) {
    var max_score_if_chosen = -999;
    for (var i in alternatives) {
      var alt = alternatives[i];
      if (alt['score_if_chosen'] > max_score_if_chosen) max_score_if_chosen = alt['score_if_chosen'];
    }
  }
  
  // Fetch all data for the report
  for (i in alternatives) {
    alt = alternatives[i];
    alt['is_chosen'] = $('#edit-tries\\[answer\\]-' + alt['id']).attr('checked');

    // Questions where multiple answers isn't allowed are scored differently...
    if (question['choice_multi'] == 0) {
      if (question['choice_boolean'] == 0) {
        if (alt['score_if_chosen'] > alt['score_if_not_chosen']) {
          alt['is_correct'] = alt['score_if_chosen'] < max_score_if_chosen ? 1 : 2;
        }
        else {
          alt['is_correct'] = 0;
        }
      }
      else {
        alt['is_correct'] = alt['score_if_chosen'] > alt['score_if_not_chosen'] ? 2 : 0;
      }
    }
    // Questions where multiple answers are allowed
    else {
      alt['is_correct'] = alt['score_if_chosen'] > alt['score_if_not_chosen'] ? 2 : 0;
    }
    var not = alt['is_chosen'] ? '' : '_not';
    alt['feedback'] = alt['feedback_if' + not + '_chosen'];
  }
  question['report'] = Multichoice.theme_report(question);
  question['score'] = Multichoice.score(question);
  question['is_correct'] = question['score'] >= question['max_score'];
};

/**
 * Score a multichoice question
 * 
 * @param question
 *  The question object
 * @return
 *  The score as an int
 */
Multichoice.score = function(question) {
  var alternatives = question['alternatives'];
  var score  = 0;
  if (question['choice_boolean']) {
    score = 1;
    for (var i in alternatives) {
      var alt = alternatives[i]; 
      if (alt['is_chosen'] && !alt['is_correct'] || !alt['is_chosen'] && alt['is_correct']) {
        score = 0;
      }
    }
  }
  else {
    for (var i in alternatives) {
      var alt = alternatives[i]; 
      if (alt['is_chosen']) {
        score += alt['score_if_chosen'];
      }
      else {
        score += alt['score_if_not_chosen'];
      }
    }
  }
  return score * question['score_weight'];
};
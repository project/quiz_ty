<?php

/**
 * Theme function for the navigation section of the quiz taking page
 */
function theme_quiz_ty_navigation() {
  $path = base_path() . drupal_get_path('module', 'quiz_ty') . '/theme/images/';
  $previous_inactive = '<img id="previous-question-inactive" class="navigation" alt="' . t('Previous question inactive') . '" title="' . t('Inactive') . '" src="' . $path . 'previous_inactive.png"/>';
  $previous = '<img id="previous-question" class="image-button navigation" alt="' . t('Previous question') . '" title="' . t('Previous question') . '" src="' . $path . 'previous.png" style="display:none;" />';
  $next = '<img id="next-question" class="image-button navigation" alt="' . t('Next question') . '" title="' . t('Next question') . '" src="' . $path . 'next.png"/>';
  $mark = '<img id="mark-questions" class="image-button navigation" alt="' . t('Mark questions') . '" title="' . t('Mark questions') . '" src="' . $path . 'mark.png" style="display:none;" />';
  $preloader = '<img id="loading-questions" alt="' . t('Loading questions') . '" title="' . t('Loading questions') . '" src="' . $path . 'preloader.gif" style="display:none;"/>';
  return $previous_inactive . $previous . $next . $mark . $preloader;
}

/**
 * Theme the quiz taking page
 *
 * @param object $first_question_node
 *  Node object for the first question in the quiz
 * @param array $question_ids
 *  array with nids and vids of all questions in the quiz
 * @param object $quiz
 *  Node object for the quiz
 * @return string themed output
 */
function theme_quiz_ty_taking($first_question_node, $question_ids, $quiz) {
  return theme('quiz_ty_progress', 0, count($question_ids))
         . '<div id="question-section">' . drupal_get_form('quiz_question_answering_form', $first_question_node, TRUE)
         . quiz_ty_set_questions_js($question_ids, $quiz)
         . theme('quiz_ty_navigation')
         . '</div>'
         . theme('quiz_ty_multichoice_report')
         . theme('quiz_ty_report');
}

/**
 * Theme a progress indicator for use during a quiz.
 *
 * @param $question_number
 *  The position of the current question in the sessions' array.
 * @param $num_questions
 *  The number of questions for this quiz as returned by quiz_get_number_of_questions().
 * @return
 *  Themed html.
 *
 * @ingroup themeable
 */
function theme_quiz_ty_progress($question_number, $num_questions) {
  // Determine the percentage finished (not used, but left here for other implementations).
  //$progress = ($question_number*100)/$num_of_question;

  // Get the current question # by adding one.
  $current_question = $question_number + 1;

  $output  = '';
  $output .= '<div id="quiz_progress">';
  $output .= t('Question <span id="quiz-question-number">@x</span><span id="quiz-of-num-questions"> (of <span id="quiz-num-questions">@y</span>)</span>:', array('@x' => $current_question, '@y' => $num_questions));
  $output .= '</div>';
  return $output;
}
<?php
/**
 * @file
 * Handles the layout of the multichoice answering form. 
 *
 *
 * Variables available:
 * - $form
 */
unset($form['tries[answer]']['#title'])
?>
<div class="multichoice-question">
<?php print drupal_render($form);?>
</div>
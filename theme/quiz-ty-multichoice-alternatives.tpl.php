<?php
/**
 * @file
 * Handles the layout of the multichoice answering form
 *
 *
 * Variables available:
 * - $form
 */
// Add script for using the entire alternative row as a button
drupal_add_js("
Drupal.behaviors.multichoiceAlternativeBehavior = function(context) {
  $('.alternative')
  .filter(':has(:checkbox:checked)')
  .addClass('selected')
  .end()
  .click(function(event) {
    $(this).toggleClass('alternative-selected');
    if ($(this).hasClass('alternative-selected')) {
      $(this).removeClass('alternative-over').addClass('alternative-selected-over');
    }
    else {
      $(this).removeClass('alternative-selected-over').addClass('alternative-over');
    }
    if (event.target.type !== 'checkbox') {
      $(':checkbox', this).attr('checked', function() {
        return !this.checked;
      });
      $(':radio', this).attr('checked', true);
      if ($(':radio', this).html() != null) {
        $('.alternative').removeClass('alternative-selected');
    	  $(this).addClass('alternative-selected').removeClass('alternative-over').addClass('alternative-selected-over');
      }
    }
  })
  .hover(function(event) {
    if ($(this).hasClass('alternative-selected')) {
      $(this).addClass('alternative-selected-over');
    }
    else {
      $(this).addClass('alternative-over');
    }
  }, function(event) {
    $(this).removeClass('alternative-selected-over').removeClass('alternative-over');
  });
};", 'inline');

// We want to have the checkbox in one table cell, and the title in the next. We store the checkbox and the titles
$options = $form['#options'];
$fullOptions = array();
$titles = array();
foreach ($options as $key => $value) {
  $fullOptions[$key] = $form[$key];
  $titles[$key] = $form[$key]['#title'];
  $fullOptions[$key]['#title'] = '';
  unset($form[$key]);
}
unset($form['#options']);
print drupal_render($form);
// We use the stored checkboxes and titles to generate a table for the alternatives
?>
<dl>
<?php
foreach ($titles as $key => $value):?>
  <div class="alternative">
    <div class="alternative-chooser"><?php print drupal_render($fullOptions[$key]);?></div>
    <div class="alternative-content"><?php print $value;?></div>
  </div>
<?php  endforeach;?>
</dl>
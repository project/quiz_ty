<?php

/**
 * Template for the report page
 *
 * This is a special template for the javascript quiz-taking engine.
 *
 * Some javascript skills might come in handy if you are to override
 * this tpl file in a complex way...
 */

/*
 * We do not need to use drupal_add_js here.
 * We don't need jQuery, we don't need the Drupal scripts and we don't need
 * to have drupal search the code for translations or themes.
 */
?>

<script type="text/javascript">
var QuizTY = QuizTY || {};

QuizTY.theme_report = function() {
  var toReturn = '<div class="quiz-report"><h2><?php print t('Question Results');?></h2>';
  toReturn += '<div id="quiz-total-score"><div id="total-score-text"><strong><?php print t('Total score is');?> ' + QuizTY.totalScore + ' <?php print t('of');?> ' + QuizTY.totalMax + '</strong></div></div>';
  toReturn += '';
  for (var i in QuizTY.questions) {
    var question = QuizTY.questions[i];
    toReturn += '<div class="question-report"><div class="quiz-report-top-section">';
    toReturn += '<p class="quiz-report-question"><strong><?php print t('Question')?> ' + (parseInt(i)+1) + ': </strong></p>';
    toReturn += question['question'];
    toReturn += '<div class="quiz-report-response-header"><strong><?php print t('Response')?>: </strong></div></div>';
    toReturn += question['report'];
    toReturn += '<div class="quiz-report-score-container"><strong>';
    toReturn += '<?php print t('Score');?> ' + question['score'] + ' <?php print t('of');?> ' + question['relative_max_score'];
    toReturn += '</strong></div>';
    toReturn += '</div>'
  }
  toReturn += '</div>';
  return toReturn;
};
</script>
<?php

/**
 * Template for the multichoice questions on the report page
 *
 * This is a special template for the javascript quiz-taking engine.
 *
 * Some javascript skills might come in handy if you are to override
 * this tpl file in a complex way...
 */

$p = drupal_get_path('module', 'quiz_ty') . '/theme/images';
$image_correct = theme(
  'image',
  "$p/correct.png",
  t('Correct'),
  t('This alternative is correct'),
  array('class' => 'feedback-icon')
);
$image_chosen = theme(
  'image',
  "$p/chosen.png",
  t('Chosen'),
  t("You chose this alternative"),
  array('class' => 'feedback-icon')
);

/*
 * We do not need to use drupal_add_js here.
 * We don't need jQuery, we don't need the Drupal scripts and we don't need
 * to have drupal search the code for translations or themes.
 */
?>
<script type="text/javascript">
var Multichoice = Multichoice || {};

Multichoice.theme_report = function(question) {
  var toReturn = '<table class="multichoice-report"><thead>';
  toReturn += '<tr><th class="column1"><?php print t('You')?></th><th class="column2"><?php print t('Correct')?></th><th class="column3"><?php print t('Alternatives')?></th></tr></thead><tbody>'
  var alternatives = question['alternatives'];
  var odd = 'tr-odd';
  for (var i in alternatives) {
    var alt = alternatives[i];
    var rowspan = 1;
    if (alt['feedback'] != null) {
      rowspan = alt['feedback'].length > 0 ? 2 : 1;
    }
    toReturn += '<tr class="' + odd + '"><td class="selector-td icon-holder column1" rowspan=' + rowspan + '>';
    if (alt['is_chosen']) {
      toReturn += <?php print drupal_to_js($image_chosen);?>;
    }
    toReturn += '</td><td class="icon-holder column2" rowspan=' + rowspan + '>';
    switch (alt['is_correct']) {
      case 1:
      case 2:
        toReturn += <?php print drupal_to_js($image_correct);?>;
      break;
    }
    toReturn += '</td><td class="column3">' + alt['answer'] + '</td></tr>';
    if (rowspan == 2) {
      toReturn += '<tr><td><strong><?php print t('Feedback:');?></strong><div class="quiz_answer_feedback">' + alt['feedback'] + '</div></td></tr>';
    }
    if (odd == 'tr-odd') {
      odd = '';
    }
    else {
      odd = 'tr-odd';
    }
  }
  toReturn += '</tbody></table>';
  return toReturn;
};

</script>
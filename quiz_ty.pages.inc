<?php 

/**
 * Page callback for taking a quiz as a self test
 */
function quiz_ty_take($quiz) {
  if (quiz_start_check($quiz, $rid)) {
    // Create question list.
    $question_ids = quiz_build_question_list($quiz);
    
    if ($question_ids === FALSE) {
      drupal_set_message(t('Not enough random questions were found. Please !add_more_questions before trying to take this @quiz.',
        array('@quiz' => QUIZ_NAME, '!add_more_questions' => l(t('add more questions'), 'node/'. arg(1) .'/questions'))), 'error');
      return '';
    }
    if (count($question_ids) == 0) {
      drupal_set_message(t('No questions were found. Please !assign_questions before trying to take this @quiz.',
        array('@quiz' => QUIZ_NAME, '!assign_questions' => l(t('assign questions'), 'node/'. arg(1) .'/questions'))), 'error');
      return t('Please assign questions...');
    }
    // Return form for the first question, add javascript file and add the question_ids as javascript
    $path_to_js = drupal_get_path('module', 'quiz_ty') . '/js/quiz_ty.js';
    drupal_add_js($path_to_js, 'module');
    drupal_add_css(drupal_get_path('module', 'quiz_ty') . '/theme/quiz_ty.css');
    
    $first_question_node = node_load($question_ids[0]['nid'], $question_ids[0]['vid']);
    
    return theme('quiz_ty_taking', $first_question_node, $question_ids, $quiz);
  }
  else {
    return t('This quiz is closed');
  }
}

/**
 * Returns a string with javascript for adding a variable to the page...
 * 
 * @param unknown_type $name
 * @param unknown_type $value
 */
function quiz_ty_set_questions_js($question_ids, $quiz) {
  drupal_add_js("$(document).ready(function() {QuizTY.setQuestions(" . drupal_to_js($question_ids) . ", $quiz->nid);});", 'inline');
}

/**
 * Ajax callback function to load the answering forms.
 * 
 * @param $quiz
 *  The quiz node
 */
function quiz_ty_load_questions_ajax($quiz) {
  $to_return = '';
  $num_loaded = 0;
  foreach ($_POST as $node_id) {
    $node = node_load($node_id['nid'], $node_id['vid']);
    if ($node) {
      $to_return .= drupal_get_form('quiz_question_answering_form', $node, TRUE);
      $num_loaded++;
    }
  }
  drupal_json(array('status' => TRUE, 'num_loaded' => $num_loaded, 'data' => $to_return));
}

/**
 * Helper function to add more data as hidden fields to the multichoice answering forms
 * 
 * @param unknown_type $form
 */
function _quiz_ty_add_multichoice_data(&$form) {
  $node = $form['#parameters'][2];
  $to_add = new stdClass();
  $to_copy = array('max_score', 'choice_multi', 'choice_random', 'choice_boolean', 'alternatives', 'type');
  foreach ($to_copy as $key) {
    $to_add->{$key} = $node->{$key};
  }
  $to_format = array('answer', 'feedback_if_chosen', 'feedback_if_not_chosen');
  foreach ($to_add->alternatives as $alt) {
    foreach ($to_format as $field) {
      $alt[$field] = check_markup($alt[$field], $alt[$field . '_format'], FALSE);
    }
  }
  $form['ty_data_' . $node->nid] = array(
    '#type' => 'hidden',
    '#value' => drupal_to_js($to_add),
  );
}